(function () {
    'use strict';

    var $canvasContainer = document.getElementById('canvas-container');
    var $scalarFieldInput = document.getElementById('scalar-field-f');
    var $scalarFieldOpRadioButtons = document.getElementsByName('scalar-field-op');
    var $btnScalarFieldPlot = document.getElementById('scalar-field-plot');
    var $vectorFieldInputX = document.getElementById('vector-field-fx');
    var $vectorFieldInputY = document.getElementById('vector-field-fy');
    var $vectorFieldInputZ = document.getElementById('vector-field-fz');
    var $vectorFieldOpRadioButtons = document.getElementsByName('vector-field-op');
    var $btnVectorFieldPlot = document.getElementById('vector-field-plot');
    var $cursorInfo = document.getElementById('plot-cursor-info');
    var $palette = document.getElementById('plot-palette');
    var $paletteCanvas = document.getElementById('plot-palette-canvas');
    var $paletteMin = document.getElementById('plot-palette-min');
    var $paletteMax = document.getElementById('plot-palette-max');
    var $zInput = document.getElementById('plot-z');
    var $valueMin = document.getElementById('value-min');
    var $valueMax = document.getElementById('value-max');
    var $arrowScale = document.getElementById('arrow-scale');
    var $arrowSpacing = document.getElementById('arrow-spacing');
    var $showAxes = document.getElementById('show-axes');

    var width = 0;
    var height = 0;

    var defaultScale = 100;
    var zoom = 1;
    var scale = defaultScale * zoom;

    var axes = null;

    var plot1 = null;
    var plot2 = null;

    var plotZ = 0;

    var paletteRangeAuto = true;
    var paletteMinSetting = 0;
    var paletteMaxSetting = 0;

    var arrowScale = 0.25;
    var arrowSpacing = 0.5;

    function getRadioButtonValue(radioButtons) {
        for (var i = 0; i < radioButtons.length; i++) {
            if (radioButtons[i].checked) {
                return radioButtons[i].value;
            }
        }

        return null;
    }

    function calculateGradient(f) {
        return {
            vx: math.derivative(f, 'x'),
            vy: math.derivative(f, 'y'),
            vz: math.derivative(f, 'z')
        };
    }

    function calculateDivergence(vx, vy, vz) {
        var dx = math.derivative(vx, 'x');
        var dy = math.derivative(vy, 'y');
        var dz = math.derivative(vz, 'z');
        return new math.expression.node.OperatorNode('+', 'add', [
            new math.expression.node.OperatorNode('+', 'add', [
                dx,
                dy
            ]),
            dz
        ]);
    }

    function calculateCurl(vx, vy, vz) {
        var dxy = math.derivative(vx, 'y');
        var dxz = math.derivative(vx, 'z');
        var dyx = math.derivative(vy, 'x');
        var dyz = math.derivative(vy, 'z');
        var dzx = math.derivative(vz, 'x');
        var dzy = math.derivative(vz, 'y');
        return {
            vx: new math.expression.node.OperatorNode('-', 'subtract', [dzy, dyz]), 
            vy: new math.expression.node.OperatorNode('-', 'subtract', [dxz, dzx]),
            vz: new math.expression.node.OperatorNode('-', 'subtract', [dyx, dxy])
        };
    }

    function compileMathJSOperatorNode(node) {
        switch (node.fn) {
            case 'add':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' + ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'subtract':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' - ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'multiply':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' * ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'divide':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' / ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'pow':
                return 'Math.pow(' + compileMathJSExpressionNode(node.args[0]) + ', ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'unaryMinus':
                return '-' + compileMathJSExpressionNode(node.args[0]);
            case 'unaryPlus':
                return compileMathJSExpressionNode(node.args[0]);
            case 'equal':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' === ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'unequal':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' !== ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'larger':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' > ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'largerEq':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' >= ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'smaller':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' < ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'smallerEq':
                return '(' + compileMathJSExpressionNode(node.args[0]) + ' <= ' + compileMathJSExpressionNode(node.args[1]) + ')';
            default:
                break;
        }

        throw new Error('Unsupported operator fn: ' + node.fn);
    }

    function compileMathJSFunctionNode(node) {
        if (node.fn.type !== 'SymbolNode') {
            throw new Error('FunctionNode fn is not a SymbolNode');
        }

        var name = node.fn.name;

        switch (name) {
            case 'abs':
                return 'Math.abs(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'cbrt':
                return 'Math.cbrt(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'ceil':
                return 'Math.ceil(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'cube':
                return 'Math.pow(' + compileMathJSExpressionNode(node.args[0]) + ', 3)';
            case 'exp':
                return 'Math.exp(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'fix':
                return 'Math.trunc(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'floor':
                return 'Math.floor(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'log':
                if (node.args.length === 1) {
                    return 'Math.log(' + compileMathJSExpressionNode(node.args[0]) + ')';
                } else {
                    return '(Math.log(' + compileMathJSExpressionNode(node.args[0]) + ') / Math.log(' + compileMathJSExpressionNode(node.args[1]) + '))';
                }
            case 'log10':
                return 'Math.log10(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'log2':
                return 'Math.log2(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'nthRoot':
                if (node.args.length === 1) {
                    return 'Math.sqrt(' + compileMathJSExpressionNode(node.args[0]) + ')';
                } else {
                    return 'Math.pow(' + compileMathJSExpressionNode(node.args[0]) + ', 1 / ' + compileMathJSExpressionNode(node.args[1]) + ')';
                }
            case 'pow':
                return 'Math.pow(' + compileMathJSExpressionNode(node.args[0]) + ', ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'round':
                return 'Math.round(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'sign':
                return 'Math.sign(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'sqrt':
                return 'Math.sqrt(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'square':
                return 'Math.pow(' + compileMathJSExpressionNode(node.args[0]) + ', 2)';
            case 'acos':
                return 'Math.acos(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'acosh':
                return 'Math.acosh(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'asin':
                return 'Math.asin(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'asinh':
                return 'Math.asinh(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'atan':
                return 'Math.atan(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'atan2':
                return 'Math.atan2(' + compileMathJSExpressionNode(node.args[0]) + ', ' + compileMathJSExpressionNode(node.args[1]) + ')';
            case 'atanh':
                return 'Math.atanh(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'cos':
                return 'Math.cos(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'cosh':
                return 'Math.cosh(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'sin':
                return 'Math.sin(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'sinh':
                return 'Math.sinh(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'tan':
                return 'Math.tan(' + compileMathJSExpressionNode(node.args[0]) + ')';
            case 'tanh':
                return 'Math.tanh(' + compileMathJSExpressionNode(node.args[0]) + ')';
            default:
                break;
        }

        throw new Error('Unsupported function: ' + name);
    }

    function compileMathJSExpressionNode(node) {
        switch (node.type) {
            case 'ConditionalNode':
                return '(' + compileMathJSExpressionNode(node.condition) + ') ? (' + compileMathJSExpressionNode(node.trueExpr) + ') : (' + compileMathJSExpressionNode(node.falseExpr) + ')';
            case 'ConstantNode':
                return node.value;
            case 'FunctionNode':
                return compileMathJSFunctionNode(node);
            case 'OperatorNode':
                return compileMathJSOperatorNode(node);
            case 'ParenthesisNode':
                return '(' + compileMathJSExpressionNode(node.content) + ')';
            case 'RelationalNode':
                break;
            case 'SymbolNode':
                var name = node.name;
                if (name === 'x' || name === 'y' || name === 'z') {
                    return name;
                } else if (typeof math[name] === 'number') {
                    return math[name];
                } else {
                    throw new Error('Symbol \'' + name + '\' is not defined');
                }
            default:
                break;
        }

        throw new Error('Unsupported node type: ' + node.type);
    }

    function compileMathJSExpressionTree(tree) {
        var simplifiedTree;
        try {
            simplifiedTree = math.simplify(tree);
        } catch (e) {
            console.warn(e);
            simplifiedTree = tree;
        }

        var jsExpression = compileMathJSExpressionNode(simplifiedTree);

        console.log('Compiled expression: ' + jsExpression);

        return new Function('x', 'y', 'z',
            'return +' + jsExpression + ';'
        );
    }

    function getPaletteColor(v, vmin, vmax, color) {
        if (isNaN(v)) {
            color.r = color.g = color.b = 0.5;
            return;
        }

        var p;
        if (v < vmin) {
            p = 0;
        } else if (v > vmax) {
            p = 1;
        } else {
            p = (v - vmin) / (vmax - vmin);
        }

        if (p < 0.25) {
            color.r = 0;
            color.g = 4 * p;
            color.b = 1;
        } else if (p < 0.5) {
            color.r = 0;
            color.g = 1;
            color.b = 2 - 4 * p;
        } else if (p < 0.75) {
            color.r = 4 * p - 2;
            color.g = 1;
            color.b = 0;
        } else {
            color.r = 1;
            color.g = 4 - 4 * p;
            color.b = 0;
        }
    }

    function Axes() {
        this.canvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext('2d');

        this.hidden = false;
    }

    Axes.prototype.setHidden = function (hidden) {
        this.hidden = hidden;

        if (hidden) {
            this.canvas.classList.add('hidden');
        } else {
            this.canvas.classList.remove('hidden');
        }
    };

    Axes.prototype.resize = function () {
        this.canvas.width = width;
        this.canvas.height = height;
    };

    Axes.prototype.redraw = function () {
        if (this.hidden) {
            return;
        }

        this.ctx.clearRect(0, 0, width, height);

        this.ctx.beginPath();

        this.ctx.moveTo(0, height / 2);
        this.ctx.lineTo(width, height / 2);

        this.ctx.moveTo(width / 2, 0);
        this.ctx.lineTo(width / 2, height);

        var spacing = 1 / Math.pow(2, Math.round(Math.log2(zoom)));

        var minPlotY = -height / 2 / scale;
        var maxPlotY = height / 2 / scale;
        var minPlotX = -width / 2 / scale;
        var maxPlotX = width / 2 / scale;

        for (var plotX = Math.floor(minPlotX / spacing) * spacing; plotX <= maxPlotX; plotX += spacing) {
            var x = width / 2 + plotX * scale;
            this.ctx.moveTo(x, height / 2 - 3);
            this.ctx.lineTo(x, height / 2 + 3);
        }

        for (var plotY = Math.floor(minPlotY / spacing) * spacing; plotY <= maxPlotY; plotY += spacing) {
            var y = height / 2 - plotY * scale;
            this.ctx.moveTo(width / 2 - 3, y);
            this.ctx.lineTo(width / 2 + 3, y);
        }

        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = '#000';
        this.ctx.stroke();

        this.ctx.font = '12px monospace';
        this.ctx.textBaseline = 'middle';

        this.ctx.textAlign = 'center';

        for (var plotX = Math.floor(minPlotX / spacing) * spacing; plotX <= maxPlotX; plotX += spacing) {
            if (plotX !== 0) {
                var x = width / 2 + plotX * scale;
                this.ctx.fillText('' + plotX, x, height / 2 + 18);
            }
        }

        this.ctx.textAlign = 'right';

        for (var plotY = Math.floor(minPlotY / spacing) * spacing; plotY <= maxPlotY; plotY += spacing) {
            if (plotY !== 0) {
                var y = height / 2 - plotY * scale;
                this.ctx.fillText('' + plotY, width / 2 - 10, y);
            }
        }
    };

    function Plot() {
        this.canvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext('2d');

        this.name = '';

        this.f = function () { return 0; };
        this.vx = function () { return 0; };
        this.vy = function () { return 0; };
        this.vz = function () { return 0; };
        this.isVectorField = false;

        this.hidden = false;

        this.imageData = null;
        this.values = [];
    }

    Plot.prototype.setHidden = function (hidden) {
        this.hidden = hidden;

        if (hidden) {
            this.canvas.classList.add('hidden');
        } else {
            this.canvas.classList.remove('hidden');
        }
    };

    Plot.prototype.setScalarField = function (name, f) {
        this.name = name;
        this.f = f;
        this.isVectorField = false;

        this.drawPalette();

        this.setHidden(false);
    };

    Plot.prototype.setVectorField = function (name, vx, vy, vz) {
        this.name = name;
        this.vx = vx;
        this.vy = vy;
        this.vz = vz;
        this.isVectorField = true;

        this.setHidden(false);
    };

    Plot.prototype.resize = function () {
        this.canvas.width = width;
        this.canvas.height = height;

        this.imageData = this.ctx.getImageData(0, 0, width, height);

        var wantedLength = width * height;
        if (this.values.length < wantedLength) {
            while (this.values.length < wantedLength) {
                this.values.push(0);
            }
        } else if (this.values.length > wantedLength) {
            this.values.length = wantedLength;
        }
    };

    Plot.prototype.drawPalette = function () {
        var canvas = $paletteCanvas;
        var ctx = canvas.getContext('2d');

        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var pixels = imageData.data;

        var i = 0;
        var color = { r: 0, g: 0, b: 0 };
        for (var y = 0; y < canvas.height; y++) {
            getPaletteColor(1 - y / canvas.height, 0, 1, color);
            for (var x = 0; x < canvas.width; x++) {
                pixels[i] = color.r * 255;
                pixels[i + 1] = color.g * 255;
                pixels[i + 2] = color.b * 255;
                pixels[i + 3] = 255;
                i += 4;
            }
        }

        ctx.putImageData(imageData, 0, 0);

        $palette.classList.remove('hidden');
    };

    Plot.prototype.redraw = function () {
        if (this.hidden) {
            return;
        }

        this.ctx.clearRect(0, 0, width, height);

        if (this.isVectorField) {
            var minPlotY = -height / 2 / scale;
            var maxPlotY = height / 2 / scale;
            var minPlotX = -width / 2 / scale;
            var maxPlotX = width / 2 / scale;

            var absArrowSpacing = arrowSpacing / Math.pow(2, Math.round(Math.log2(zoom)));
            var absArrowScale = arrowScale / Math.pow(2, Math.round(Math.log2(zoom)));

            this.ctx.fillStyle = '#000';

            for (var plotY = Math.floor(minPlotY / absArrowSpacing) * absArrowSpacing; plotY <= maxPlotY; plotY += absArrowSpacing) {
                for (var plotX = Math.floor(minPlotX / absArrowSpacing) * absArrowSpacing; plotX <= maxPlotX; plotX += absArrowSpacing) {
                    var x = width / 2 + plotX * scale;
                    var y = height / 2 - plotY * scale;
                    this.ctx.beginPath();
                    this.ctx.arc(x, y, 2, 0, Math.PI * 2);
                    this.ctx.fill();
                }
            }

            this.ctx.beginPath();

            for (var plotY = Math.floor(minPlotY / absArrowSpacing) * absArrowSpacing; plotY <= maxPlotY; plotY += absArrowSpacing) {
                for (var plotX = Math.floor(minPlotX / absArrowSpacing) * absArrowSpacing; plotX <= maxPlotX; plotX += absArrowSpacing) {
                    var vxvalue = this.vx(plotX, plotY, plotZ);
                    var vyvalue = this.vy(plotX, plotY, plotZ);
                    // var vzvalue = this.vz(plotX, plotY, plotZ);
                    if (vxvalue !== 0 || vyvalue !== 0) {
                        var startX = width / 2 + plotX * scale;
                        var startY = height / 2 - plotY * scale;
                        var endX = width / 2 + (plotX + vxvalue * absArrowScale) * scale;
                        var endY = height / 2 - (plotY + vyvalue * absArrowScale) * scale;
                        this.ctx.moveTo(startX, startY);
                        this.ctx.lineTo(endX, endY);
                        var headLength = 10;
                        var angle = Math.atan2(endY - startY, endX - startX);
                        this.ctx.moveTo(endX, endY);
                        this.ctx.lineTo(endX - headLength * Math.cos(angle - Math.PI / 6), endY - headLength * Math.sin(angle - Math.PI / 6));
                        this.ctx.moveTo(endX, endY);
                        this.ctx.lineTo(endX - headLength * Math.cos(angle + Math.PI / 6), endY - headLength * Math.sin(angle + Math.PI / 6));
                    }
                }
            }

            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = '#000';
            this.ctx.lineCap = 'round';
            this.ctx.stroke();
        } else {
            var fmin = Infinity;
            var fmax = -Infinity;

            var i = 0;
            for (var y = 0; y < height; y++) {
                var plotY = (height - y - height / 2) / scale;
                for (var x = 0; x < width; x++) {
                    var plotX = (x - width / 2) / scale;
                    var fvalue = this.f(plotX, plotY, plotZ);
                    if (fvalue < fmin) {
                        fmin = fvalue;
                    }
                    if (fvalue > fmax) {
                        fmax = fvalue;
                    }
                    this.values[i] = fvalue;
                    i++;
                }
            }

            var paletteMin;
            var paletteMax;
            if (paletteRangeAuto) {
                paletteMin = fmin;
                paletteMax = fmax;
                if (paletteMin === paletteMax) {
                    paletteMin -= 1;
                    paletteMax += 1;
                }
            } else {
                paletteMin = paletteMinSetting;
                paletteMax = paletteMaxSetting;
            }

            $paletteMin.textContent = paletteMin.toFixed(3);
            $paletteMax.textContent = paletteMax.toFixed(3);

            var pixels = this.imageData.data;

            var i = 0;
            var j = 0;
            var color = { r: 0, g: 0, b: 0 };
            for (var y = 0; y < height; y++) {
                for (var x = 0; x < width; x++) {
                    var fvalue = this.values[j];
                    getPaletteColor(fvalue, paletteMin, paletteMax, color);
                    pixels[i] = color.r * 255;
                    pixels[i + 1] = color.g * 255;
                    pixels[i + 2] = color.b * 255;
                    pixels[i + 3] = 255;
                    i += 4;
                    j++;
                }
            }

            this.ctx.putImageData(this.imageData, 0, 0);
        }
    };

    function resize() {
        width = $canvasContainer.offsetWidth;
        height = $canvasContainer.offsetHeight;
        plot1.resize();
        plot2.resize();
        axes.resize();
    }

    function redraw() {
        plot1.redraw();
        plot2.redraw();
        axes.redraw();
    }

    window.addEventListener('resize', function () {
        resize();
        redraw();
    });

    function plotScalarField(fStr, op) {
        var f = math.parse(fStr);

        switch (op) {
            case 'none':
                plot1.setScalarField('f(x, y, z)', compileMathJSExpressionTree(f));
                plot2.setHidden(true);
                break;
            case 'grad':
                plot1.setScalarField('f(x, y, z)', compileMathJSExpressionTree(f));
                var grad = calculateGradient(f);
                plot2.setVectorField('v(x, y, z)', compileMathJSExpressionTree(grad.vx), compileMathJSExpressionTree(grad.vy), compileMathJSExpressionTree(grad.vz));
                break;
        }

        if (!plot1.isVectorField) {
            plot1.drawPalette($paletteCanvas);
        } else if (!plot2.isVectorField) {
            plot2.drawPalette($paletteCanvas);
        }

        if (!plot1.isVectorField || !plot2.isVectorField) {
            $palette.classList.remove('hidden');
        } else {
            $palette.classList.add('hidden');
        }

        redraw();
    }

    $btnScalarFieldPlot.addEventListener('click', function () {
        var fStr = $scalarFieldInput.value || '0';
        var op = getRadioButtonValue($scalarFieldOpRadioButtons);

        try {
            plotScalarField(fStr, op);
        } catch (e) {
            alert(e.toString());
        }
    });

    function plotVectorField(vxStr, vyStr, vzStr, op) {
        var vx = math.parse(vxStr);
        var vy = math.parse(vyStr);
        var vz = math.parse(vzStr);

        switch (op) {
            case 'none':
                plot1.setVectorField('v(x, y, z)', compileMathJSExpressionTree(vx), compileMathJSExpressionTree(vy), compileMathJSExpressionTree(vz));
                plot2.setHidden(true);
                $palette.classList.add('hidden');
                break;
            case 'div':
                var div = calculateDivergence(vx, vy, vz);
                plot1.setScalarField('div(v)', compileMathJSExpressionTree(div));
                plot2.setVectorField('v(x, y, z)', compileMathJSExpressionTree(vx), compileMathJSExpressionTree(vy), compileMathJSExpressionTree(vz));
                break;
            case 'rot':
                plot1.setVectorField('v(x, y, z)', compileMathJSExpressionTree(vx), compileMathJSExpressionTree(vy), compileMathJSExpressionTree(vz));
                var curl = calculateCurl(vx, vy, vz);
                plot2.setVectorField('rot(v)', compileMathJSExpressionTree(curl.vx), compileMathJSExpressionTree(curl.vy), compileMathJSExpressionTree(curl.vz));
                $palette.classList.add('hidden');
                break;
            case 'rotz':
                var curl = calculateCurl(vx, vy, vz);
                plot1.setScalarField('rot(v)_z', compileMathJSExpressionTree(curl.vz));
                plot2.setVectorField('v(x, y, z)', compileMathJSExpressionTree(vx), compileMathJSExpressionTree(vy), compileMathJSExpressionTree(vz));
                break;
        }

        redraw();
    }

    $btnVectorFieldPlot.addEventListener('click', function () {
        var vxStr = $vectorFieldInputX.value || '0';
        var vyStr = $vectorFieldInputY.value || '0';
        var vzStr = $vectorFieldInputZ.value || '0';
        var op = getRadioButtonValue($vectorFieldOpRadioButtons);

        try {
            plotVectorField(vxStr, vyStr, vzStr, op);
        } catch (e) {
            alert(e.toString());
        }
    });

    function getPlotValueHTML(plot, x, y, z) {
        if (plot.hidden) {
            return '';
        }

        if (plot.isVectorField) {
            return plot.name + '_x = ' + plot.vx(x, y, z).toFixed(3) + '<br />' +
                plot.name + '_y = ' + plot.vy(x, y, z).toFixed(3) + '<br />' + 
                plot.name + '_z = ' + plot.vz(x, y, z).toFixed(3) + '<br />';
        } else {
            return plot.name + ' = ' + plot.f(x, y, z).toFixed(3) + '<br />';
        }
    }

    $canvasContainer.addEventListener('mousemove', function (evt) {
        var box = $canvasContainer.getBoundingClientRect();

        var x = evt.clientX - box.left;
        var y = evt.clientY - box.top;

        var plotX = (x - box.width / 2) / scale;
        var plotY = (box.height / 2 - y) / scale;

        $cursorInfo.innerHTML = 'x = ' + plotX.toFixed(3) + '<br />' +
            'y = ' + plotY.toFixed(3) + '<br />' +
            'z = ' + plotZ + '<br />' +
            getPlotValueHTML(plot1, plotX, plotY, plotZ) +
            getPlotValueHTML(plot2, plotX, plotY, plotZ);

        $cursorInfo.classList.remove('hidden');
    });

    $canvasContainer.addEventListener('mouseleave', function () {
        $cursorInfo.classList.add('hidden');
    });

    var zoomInFactor = 1.1;
    var zoomOutFactor = 1 / zoomInFactor;
    $canvasContainer.addEventListener('wheel', function (evt) {
        if (evt.deltaY !== 0) {
            var zoomFactor = evt.deltaY < 0 ? zoomInFactor : zoomOutFactor;
            zoom *= zoomFactor;
            scale = defaultScale * zoom;

            redraw();
        }
    });

    $zInput.addEventListener('input', function () {
        plotZ = +$zInput.value;

        redraw();
    });

    $valueMin.addEventListener('input', function () {
        if ($valueMin.value !== '') {
            paletteMinSetting = +$valueMin.value;
            if ($valueMax.value !== '') {
                paletteRangeAuto = false;
            }
        } else {
            paletteRangeAuto = true;
        }

        redraw();
    });

    $valueMax.addEventListener('input', function () {
        if ($valueMax.value !== '') {
            paletteMaxSetting = +$valueMax.value;
            if ($valueMin.value !== '') {
                paletteRangeAuto = false;
            }
        } else {
            paletteRangeAuto = true;
        }

        redraw();
    });

    $arrowScale.addEventListener('input', function () {
        arrowScale = +$arrowScale.value;

        redraw();
    });

    $arrowSpacing.addEventListener('input', function () {
        var newArrowSpacing = +$arrowSpacing.value;

        if (newArrowSpacing !== 0) {
            arrowSpacing = newArrowSpacing;
        }

        redraw();
    });

    $showAxes.addEventListener('input', function () {
        axes.setHidden(!$showAxes.checked);

        axes.redraw();
    });

    plot1 = new Plot();
    $canvasContainer.appendChild(plot1.canvas);

    plot2 = new Plot();
    $canvasContainer.appendChild(plot2.canvas);

    axes = new Axes();
    $canvasContainer.appendChild(axes.canvas);

    resize();

    plot1.setHidden(true);
    plot2.setHidden(true);

    redraw();
})();
