
# FieldViewer

This application allows viewing scalar and vector fields, and the resulting fields of the grad, div and rot operators.

## Used libraries

- Math.js (http://mathjs.org/)

